package com.iacademy.midterms_magsalin;

import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import org.w3c.dom.Text;
import java.text.NumberFormat;

public class MainActivity extends AppCompatActivity {

    String hungry = "I'm so hungry";
    String full = "I'm so full";

    private int quantity = 1;

    private int price = 180;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.midterm_form);
    }

    public void setText(){

            TextView hungry = (TextView) findViewById(R.id.textView);
            hungry.setText(full);

    }

    public void setText2(){

        TextView full = (TextView) findViewById(R.id.textView);
        full.setText(hungry);

    }

    public void setImageResource(){

        ImageView imageFull = (ImageView) findViewById(R.id.imageView);
        imageFull.setImageResource(R.drawable.after_cookie);
    }

    public void setImageResource2(){

        ImageView imageHungry = (ImageView) findViewById(R.id.imageView);
        imageHungry.setImageResource(R.drawable.before_cookie);
    }
    public void eatCookie(View view){

        setText();
        setImageResource();
    }

    public void reset2(View view){

        setText2();
        setImageResource2();
    }

    public void submitOrder(View v){
        displayPrice(quantity * price);
    }

    public void displayPrice(int number){
        TextView price = (TextView) findViewById(R.id.textView6);
        price.setText(NumberFormat.getCurrencyInstance().format(number));
    }

    public void increment(View view){
        quantity++;
        displayQuantity(quantity);
    }

    public void decrement(View view){
        quantity--;
        displayQuantity(quantity);
    }

    public void displayQuantity(int number){
        TextView quantity = (TextView) findViewById(R.id.textView5);
        quantity.setText("" + number);
    }

    public void reset3(View view){
        quantity = 1;
        price = 180;
        displayQuantity(quantity);
        displayPrice(price);
    }

    public void isWhippedCreamChecked(View view){


    }

    public void isChocolateChecked(View view){

    }

    public void calculatePrice(View view){


    }

    public void createOrderSummary(View view){

    }
}
